FROM ubuntu:latest


RUN apt-get update
RUN apt-get -qq -y install curl
RUN apt-get -qq -y install python3 python3-dev python3-pip
RUN pip3 install qiskit tqdm
COPY . /app/

RUN pip3 install -r /app/requirements.txt

