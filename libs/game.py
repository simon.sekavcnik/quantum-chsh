# ENTANGLEMENT-ENABLED COMMUNICATION
from datetime import datetime, date
import os
from qiskit import *
from qiskit.quantum_info.synthesis import euler_angles_1q
from math import *
from random import choices
import numpy as np
from operator import xor # Can be removed
from tqdm import tqdm

class Player:
    instances = 0
    def __init__(self, name, thetas=None, angles=None):
        self.name = name
        self.index = Player.instances
        self.thetas = thetas
        Player.instances = Player.instances +1
        self.measured_state = None
        self.received_bit = None
        self.unitary_matrices = []
        #Check input
        if(thetas==None and angles==None):
            raise Exception('Thetas or angles have to be provided!')
        elif(thetas!=None and angles!=None):
            raise Exception('Thetas OR angles have to be provided, not both')
        
        if(thetas!=None):
            self._unitaries_from_thetas(thetas)
        else:
            self._unitaries_from_angles(angles)



    def __str__(self):
        return self.name + " index:" + str(self.index)

    def get_angles(self):
        if(self.received_bit == None):
            raise Exception('Bits must be received in order to construct unitary gate!')

        matrix = self.unitary_matrices[self.received_bit]
        angles = euler_angles_1q(matrix)
        return angles

    def receive_bit(self, bit):
        self.received_bit = bit

    def _unitaries_from_thetas(self, thetas):
        for theta in thetas:
            self.unitary_matrices.append(np.array([
                [np.cos(theta), np.sin(theta)],
                [-np.sin(theta), np.cos(theta)]]))

    def _unitaries_from_angles(self, angles):
        for angle in angles:
            x = (angle[0]+angle[2])/2
            y = (angle[0]-angle[2])/2
            b = angle[1]/2
            matrix = np.array([
                [(np.cos(-x)+1j*sin(-x))*cos(b), -(np.cos(-y)+1j*np.sin(-y))*sin(b)],
                [(np.cos(-y)+1j*sin(-y))*sin(b), (np.cos(x)+1j*sin(x))*cos(b)]
                ])
            self.unitary_matrices.append(matrix)


class Game:
    def __init__(self, players, distribution=None):
        self.debug = False
        self.players = players
        self.quantum_circuit = QuantumCircuit(len(self.players))
        self.sent_bits = []
        self.received_bits = []
        self.simulation_results = None
        self.game_result = None
        self.strings = []
        self.string_probabilities = []
        if distribution!=None:
            #Check if input is correct
            self.strings = distribution[0]
            self.string_probabilities = distribution[1]
        else: #Generate normal distribution
            for i in range(2**len(players)):
                self.strings.append("{0:b}".format(i).zfill(len(players)))
                self.string_probabilities.append(2**(-len(players)))

    def entangle_all(self):
        self.quantum_circuit.h(0)
        for i in range(1,len(self.players)):
            self.quantum_circuit.cx(0,i)
        self.quantum_circuit.barrier(range(len(self.players)))
        
    def measure_all(self):
        N = len(self.players)
        meas = QuantumCircuit(N,N)
        meas.barrier(range(N))
        meas.measure(range(N),range(N))
        self.quantum_circuit = self.quantum_circuit + meas
        if self.debug:
            print(self.quantum_circuit)

    def simulate(self): #Execute the Qubit Simulation
        backend_sim = Aer.get_backend('qasm_simulator')
        job_sim = execute(self.quantum_circuit, backend_sim, shots=1)
        self.results = job_sim.result().get_counts(self.quantum_circuit)

    def generate_and_send_bits(self):
        rand_string = choices(self.strings, weights=self.string_probabilities)[0]
        for i in range(len(self.players)):
            self.players[i].receive_bit(int(rand_string[i]))
            self.sent_bits.append(int(rand_string[i]))
        self._print("Sent bits: ", self.sent_bits)

    def generate_and_append_unitaries(self):
        N = len(self.players)
        unitaries = QuantumCircuit(N,N)
        #unitaries.barrier(range(N))
        for i in range(N):
            angles = self.players[i].get_angles()
            self._print(angles)
            unitaries.u3(angles[0],angles[1],angles[2],i)
        self.quantum_circuit = self.quantum_circuit + unitaries

    def evaluate(self):
        if self.results == None:
            raise Exception("Run simulation first!")
        #print("Results: " + str(self.results))
        self.received_bits = list(list(self.results.keys())[0])
        
        #self._print(self.sent_bits)
        #self._print(self.received_bits)

        for i in range(len(self.received_bits)):
            self._print(self.received_bits[i])
            self.received_bits[i] = bool(int(self.received_bits[i]))
            self.sent_bits[i] = bool(int(self.sent_bits[i]))

        self._print("Sent bits: ", self.sent_bits)
        self._print("Received_bits: ", self.received_bits)

        # WIN CONDITIONS
        #a = self.sent_bits[0]*self.sent_bits[1]
        a=1
        for i in range(len(self.sent_bits)):
            a = a*self.sent_bits[i]

        # Mapping {0,1} to {-1,1} and back for multivar XOR
        b=1
        for i in range(len(self.received_bits)):
            if self.received_bits[i] == 0:
                b = b*1
            else:
                b = b*(-1)
        if b == 1:
            b = 0
        else:
            b = 1


        self._print(a,b)
        if(a==b):
            self._print("Game won")
            self.game_result = True
        else:
            self._print("Game lost")
            self.game_result = False

    def _print(self, *content):
        if( self.debug ):
            print(content)

    def run_game(self):
        self.generate_and_send_bits()
        self.entangle_all()
        self.generate_and_append_unitaries()
        self.measure_all()
        self.simulate()
        self.evaluate()
