# Helpers used by coordinator.py
from os import path, makedirs
from pathlib import Path
from fractions import Fraction
import json, csv

# Checks the config file,
def check_config_file(dest):
    if not path.exists(dest):
        raise Exception("Config file doesn't exist\n Destination checked:"+dest)
    return True

#Reads the parameters and returns object
def get_parameters_from_config(dest):
    if not path.exists(dest):
        raise Exception("Config file doesn't exist\n Destination checked:"+dest)
    with open(dest) as config_file:
        data = json.load(config_file)
        data = data["simulations"]
        activeSim = None
        #Find first uncompleted simulation
        for simulation in data:
            if not simulation["isFinished"]:
                return simulation
                activeSim = simulation
                break
        return False


#Finds Log file or creates it if it doesn't exist
def create_log_file(dest):

    #Create folder if it doesn't exist already
    try:
        makedirs("/".join(dest.split("/")[:-1]))
    except FileExistsError:
        print("File exists already")

    #Create a file if it doesn't exist already
    if not path.exists(dest):
        open(dest,'w').close()
    
def get_simulation_params(conf,log_file):
    #Generate starting angles
    params = []
    angles_per_player = 2 if conf["isUnitaryReal"] else 3
    for x in range(conf["numberOfPlayers"]):
        ap = []
        for y in range(angles_per_player):
            ap.append("-1/1")
        params.append(ap)
    print(params)

    flag=True
    while flag:
        print("iterating")

        f = open(log_file)
        csv_f = csv.reader(f, delimiter="|")
        for row in csv_f:
            print("Inner loop")
            print(len(row))
            if len(row)<3:
                print(row)
                print("Is last")
                flag=False
                break
            print("ROW")
            print(row[3])
            print(params)
            print(_angles_str2lst(row[3]))
            if _angles_str2lst(row[3])==params:
                params=_get_next_params(params,conf["currentResolution"])
                flag=False
                break
    flag=True
    return params

def _is_last(itr):
    old = next(itr)#itr.next()
    for new in itr:
        yield False, old
        old = new
    yield True, old

def _get_next_params(params, res):
    res = res.split("/")
    res = Fraction(int(res[0]),int(res[1]))
    print("Get next angles")
    for i,player in enumerate(params):
        for x,angle in enumerate(player):
            print(angle)
            angle = angle.split("/")
            angle = Fraction(int(angle[0]),int(angle[1]))
            angle = angle + res
            print(angle)
            if angle > 1:
                params[i][x] = "-1/1"
            else:
                params[i][x] = str(angle.numerator)+"/"+str(angle.denominator)
            if angle != "-1/1":
                break
        else: 
            continue
        break
    return params
    
def _angles_str2lst(a_string):
    angles = a_string.split(";")
    answer = []
    for a in angles:
        b = []
        a = a.split(",")
        for c in a:
            b.append(c)
        answer.append(b)
    print(answer)
    return answer
