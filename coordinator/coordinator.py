# This script is run by coordinator/index.js
# ------------------------------------------
# It produces log files and parameters to be sent to each node
#
import argparse, sys
import json
from pylibs.helpers import *

parser = argparse.ArgumentParser()
parser.add_argument('--t',
        help="Get parameters for node, or log results",
        required=True)

args = vars(parser.parse_args())

# CONFIG PARAMETERS
CONFIG_FILE = "config.json"
LOG_DIRECTORY = "../logs/"
# END OF CONFIG PARAMS

# Produces parameters for each node to run
def get_parameters():
    print("Produce simulation Parameters")
    config_data = get_parameters_from_config(CONFIG_FILE)
    create_log_file(LOG_DIRECTORY+config_data["logFile"])
    #This function probably should not be trusted
    params =get_simulation_params(config_data, LOG_DIRECTORY+config_data["logFile"]) 
    print(params)
    return(params)

# Logs the received results (Produces log files | csv)
def log_results(results):
    pass

if args["t"] == "g":
    get_parameters()
elif args["t"] == "l":
    log_results()

