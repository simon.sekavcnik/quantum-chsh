const WebSocket = require('ws');
console.log("Starting the server!");

function noop() {}

function heartbeat() {
  this.isAlive = true;
}

const wss = new WebSocket.Server({ port: 8050 });

wss.on('connection', function connection(ws) {
  console.log('Connection Request received');
  ws.isAlive = true;
  ws.on('pong', heartbeat);
  ws.on('message', (str)=>{
    console.log(str);
  });
});

const interval = setInterval(function ping() {
  console.log("Testing connection for "+ wss.clients.size +" connections.");
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false){
      console.log("Connection terminated!");
      return ws.terminate();
    }
    ws.send("Heartbeat");
    ws.isAlive = false;
    ws.ping(noop);
  });
}, 30000);

