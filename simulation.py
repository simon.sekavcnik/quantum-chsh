from libs.game import *
from datetime import datetime
import argparse, sys
import json
import time

class Simulation:
    def __init__(self):
        pass

parser = argparse.ArgumentParser()
parser.add_argument('--num-of-players', 
        help="How many participating players",
        required=True)
parser.add_argument('--matrix-input-type', 
        help="Type of matrix entry (0->thetas,1->angles",
        required=True)
parser.add_argument('--unitaries', 
        help="Python array for construction of unitary matrices, based on specified input",
       required=True )
parser.add_argument('--strings', 
        help="Strings with each character 0 or 1",
        required=True)
parser.add_argument('--string-probabilities',
        help="Probability distribution for defined strings",
        required=True)
parser.add_argument('--sim-amount', 
        help="How many games should be simulated",
        required=True)
args = vars(parser.parse_args())

start_time = time.time()

#Getting amount of games to be simulated
num_of_games = int(args["sim_amount"])


#Getting amount of players for simulations
num_of_players = int(args["num_of_players"])


#Building angles or thetas from input
m_inputs = args["unitaries"].split(";")
angles = []
pi_angles = []
for m_input in m_inputs: 
    m_input = m_input.split(",")
    m = []
    m_pi = []
    for i in m_input:
        if '/' in i:
            equation = list(map(float,i.split('/')))
            i = equation[0]/equation[1]
        m.append(pi*float(i))
        m_pi.append(float(i))
    angles.append(m)
    pi_angles.append(m_pi)


#Building Strings with distributions
distribution = []
distribution.append(args["strings"].split(","))
distribution.append(args["string_probabilities"].split(","))
if(len(distribution[0])!=len(distribution[1])):
    raise Exception("Strings and string distribution should have equal amount of elements")
for x in range(len(distribution[0])):
    distribution[0][x] = str(distribution[0][x])
for x in range(len(distribution[1])):
    distribution[1][x] = float(distribution[1][x])


#Starting Simulation

wins = 0
for i in range(num_of_games):

    players = []
    for p in range(num_of_players):
        if int(args["matrix_input_type"])==0:
            players.append(Player("p"+str(p), thetas=angles[p]))
        elif int(args["matrix_input_type"])==1:
            players.append(Player("p"+str(p), angles=angles[p]))
    ''' 
    players.append(Player("p1", thetas=[0,pi/4]))
    players.append(Player("p2", thetas=[pi/8,-pi/8]))
    '''
    game = Game(players,distribution=distribution )
    #game.debug = True
    game.run_game()
    if game.game_result:
        wins = wins +1
print(wins)


#Code for reporting to coordinator

players = []
for p in range(num_of_players):
    if int(args["matrix_input_type"])==0:
        players.append(Player("p"+str(p), thetas=angles[p]))
    elif int(args["matrix_input_type"])==1:
        players.append(Player("p"+str(p), angles=angles[p]))

execution_time = time.time() - start_time

data = {}
data["timestamp"]           = str(datetime.now())
data["simulated-by"]        = "Entity Name"
data["execution-time"]      = execution_time
data["games-played"]        = num_of_games
data["wins"]                = wins
data["win-percentage"]      = "{0:.2f}".format(100*wins/num_of_games)
data["number-of-players"]   = num_of_players
data["unitary-matrix-type"] = "real" if (int(args["matrix_input_type"])==0) else "complex"
data["players"]             = []

for index, p in enumerate(players):
    player = {}
    player["angles"] = angles[index]
    player["angles-pi"] = pi_angles[index]
    player["matrices"] = []
    for i in p.unitary_matrices:
        player["matrices"].append(i.tolist())
    data["players"].append(player)

print(json.dumps(data,indent=2, sort_keys=True))
sys.stdout.flush()
