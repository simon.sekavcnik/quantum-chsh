const WebSocket = require('ws');
let {PythonShell} = require('python-shell')
console.log("calling python from node.js")
var simulationScript = "../simulation.py";

var options = {
  mode: 'text',
  args: ["--num-of-players=2",
    "--matrix-input-type=0",
    "--unitaries=0,1/4;1/8,-1/8",
    "--strings=00,01,10,11",
    "--string-probabilities=0.25,0.25,0.25,0.25",
    "--sim-amount=100"]
}

var isRunning = false;

isRunning = true;
PythonShell.run(simulationScript, options, function(err,results){
  if(err) throw err;
  console.log(results);
  isRunning = false;
});

console.log("Is this asynchronous?");




  
  /*
const WebSocketAPI = "ws://localhost:8050";

function heartbeat() {
  clearTimeout(this.pingTimeout);
  console.log("PING RECEIVED");

  this.send("Status");

  this.pingTimeout = setTimeout(() => {
    this.terminate();
  }, 30000 + 1000);
}


const client = new WebSocket(WebSocketAPI);

client.on('open', heartbeat);
client.on('ping', heartbeat);
client.on('close', function clear() {
  clearTimeout(this.pingTimeout);
});

*/
