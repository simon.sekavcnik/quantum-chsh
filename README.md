# N Partite Quantum XOR Game Simultaion

Setup is programmed in a way where simulations are coordinated by central
coordinator (raspberry pi) on any number of machines.

Communication is done trough websockets.

To start the client use the following commands:
 
```
docker build -t . qxor
docker run -t -d --name qxor qxor /bin/bash
docker ps
```
To attach to running container use the following commands:
```
docker exec -it qxor bash    
```


